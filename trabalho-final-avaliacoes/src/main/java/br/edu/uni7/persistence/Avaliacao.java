package br.edu.uni7.persistence;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity 
@Table(name = "TBL_AVALIACOES")
public class Avaliacao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PK_AVAL")
	private Long id;
	
	@Column(name = "DT_DATA_AVALIACAO")
	private Date data;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_PRO")
	@NotNull
	private Produto produto;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_USU")
	@NotNull
	private Usuario autor;	
	
	@OneToMany
	@JoinColumn(name = "FK_AVAL")
	@Min(1)
	private List<ItemAvaliacao> itensAvaliacao;
	
	@PrePersist @PreUpdate
	public void verificaDataAvaliacaoPreenchidaInserir() throws Exception{
		if(getData() == null && getData().toString().isEmpty())
			throw new Exception("Data Nula");
	}
	
	@PrePersist @PreUpdate
	public void verificaStatusItemAvaliacaoComoAberto() throws Exception{
		for (ItemAvaliacao itemAvaliacao : itensAvaliacao) {
			if(itemAvaliacao.getStatus().equals("FECHADO"))
				throw new Exception("O status da Avaliacao deve estar ABERTO");
		}
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Usuario getAutor() {
		return autor;
	}

	public void setAutor(Usuario autor) {
		this.autor = autor;
	}

	public List<ItemAvaliacao> getItensAvaliacao() {
		return itensAvaliacao;
	}

	public void setItensAvaliacao(List<ItemAvaliacao> itensAvaliacao) {
		this.itensAvaliacao = itensAvaliacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Avaliacao other = (Avaliacao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
